import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import IEfetch from 'isomorphic-fetch';
import CustomFun from "./plugins/CustomFun";

Vue.config.productionTip = false;
Vue.use(IEfetch);
Vue.prototype.custom_fun = CustomFun;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
