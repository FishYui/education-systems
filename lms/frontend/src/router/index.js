import Vue from "vue";
import VueRouter from "vue-router";
import CustomFun from "../plugins/CustomFun";

//views
//private
import PirvateIndex from "../views/private/Index.vue";
import MyCoursePage from "../views/private/MyCoursePage.vue";
import CourseContentPage from "../views/private/CourseContentPage.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: PirvateIndex,
    children: [
      {
        path: "/",
        name: "Home",
        component: MyCoursePage,
        meta: {
          auth: true
        }
      },
      {
        path: "/content/:cid",
        name: "CourseContent",
        component: CourseContentPage,
        meta: {
          auth: true
        }
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {

  if(to.query.token) {
    await CustomFun.fetch(`${process.env.VUE_APP_LMS_SERVER}/login`, {
      method: "POST",
      headers: {
        "content-type": "application/json",
        "authorization": to.query.token
      },
      credentials: 'include'
    });
  }

  if(to.meta.auth === true) {
    const auth_res = await CustomFun.fetch(`${process.env.VUE_APP_LMS_SERVER}/user/isLogin`, { credentials: 'include' });
    if(auth_res.success) 
      next();
  }
  else
    next();
});

export default router;
