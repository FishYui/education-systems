const jwt = require('../helpers/jwt');
const GeneralError = require('../helpers/error');

module.exports = async (req, res, next) => {
  const authToken = req.headers.authorization || req.cookies.Authentication;

  if (!authToken) {
    const newError = new GeneralError('AUTH-0001');
    return next(newError);
  }

  const [type, token] = authToken.split(' ');
  if (type != 'Bearer' || !token) {
    const newError = new GeneralError('AUTH-0002');
    return next(newError);
  }

  try {
    const payload = await jwt.verify(token);
    req.thisUser = {
      jwtPayload: payload,
    };
  } catch (error) {
    let newError;
    if (error.name === 'TokenExpiredError') {
      newError = new GeneralError('AUTH-0002');
    } else {
      newError = new GeneralError('AUTH-0001');
    }

    return next(newError);
  }

  next();
};
