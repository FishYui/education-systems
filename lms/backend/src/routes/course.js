const express = require('express');
const router = express.Router();

// middlewares
const AuthHandle = require('../middlewares/authHandle');

// controllers
const slotControllers = require('../controllers/course');

// /course/  => 取得個人的課程清單
router.get('/',
  AuthHandle,
  slotControllers.myCourse);

// /course/content/:courseId  => 取得特定課程編號的教材清單
router.get('/content/:courseId',
  AuthHandle,
  slotControllers.courseContent);

module.exports = router;
