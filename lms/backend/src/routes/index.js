const user = require('./user');
const course = require('./course');

const routes = (app) => {
  app.use('/user', user);
  app.use('/course', course);
};

module.exports = routes;
