const express = require('express');
const router = express.Router();

// middlewares
const AuthHandle = require('../middlewares/authHandle');

// controllers
const userControllers = require('../controllers/user');

router.get('/isLogin',
  AuthHandle,
  userControllers.isLogin);

module.exports = router;
