const dotenv = require('dotenv');

const envFound = dotenv.config();
if (envFound.error) {
  throw new Error('⚠️  Couldn\'t find .env file  ⚠️');
}

module.exports = {

  port: process.env.PORT || 9002,

  fhirServer: process.env.FHIR_SERVER,

  secret: {
    jwt: {
      type: process.env.JWTTYPE, // key or secret
      publicKey: '',
      privateKey: '',
      secret: process.env.JWTSECRET,

      issuer: 'https://mitw.portal',
      audience: ['https://mitw.portal', 'https://mitw.lms', 'https://mitw.css'],
      expiration: 30 * 60,
    },
  },

  systemUrl: {
    portalClient: process.env.PORTALCLIENT,
    portalServer: process.env.PORTALSERVER,
    lmsClient: process.env.LMSCLIENT,
    lmsServer: process.env.LMSSERVER,
  },

};
