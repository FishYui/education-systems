class GeneralError extends Error {
  constructor(errCode) {
    super();
    this.errorCode = errCode;
    this.message = '';
    this.statusCode = 0;
    this.getErrorDetail(this.errorCode);
  }

  getErrorDetail(errCode) {
    const {message, statusCode} = errors.find((err) => err.errorCode === errCode);
    this.message = message;
    this.statusCode = statusCode;
  }
}

module.exports = GeneralError;

const errors = [
  {
    errorCode: 'INPUT-0001',
    message: '資料輸入有誤。',
    statusCode: 400,
  },

  {
    errorCode: 'AUTH-0001',
    message: '請先登入。',
    statusCode: 401,
  },
  {
    errorCode: 'AUTH-0002',
    message: '登入時間已過，請重新登入。',
    statusCode: 401,
  },
  {
    errorCode: 'AUTH-0003',
    message: '未經授權，無法使用。',
    statusCode: 403,
  },

  {
    errorCode: 'UNKN-0001',
    message: '系統內部發生未知錯誤。',
    statusCode: 500,
  },
];
