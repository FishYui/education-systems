const jsonwebtoken = require('jsonwebtoken');
const config = require('../config/index');
const {v4: uuidv4} = require('uuid');

const jwtConfig = config.secret.jwt;
const jwtAlg = jwtConfig.type === 'secret' ? 'HS256' : 'RS256';
const publicKey = jwtConfig.type === 'secret' ? jwtConfig.secret : jwtConfig.publicKey;
const privateKey = jwtConfig.type === 'secret' ? jwtConfig.secret : jwtConfig.privateKey;


// 核發
exports.sign = (payload, exp = jwtConfig.expiration, key = privateKey) => {
  const pay = {
    iat: Math.floor(Date.now() / 1000),
    jti: uuidv4(),
    iss: jwtConfig.issuer,
    ...payload,
  };
  const opt = {
    algorithm: jwtAlg,
    expiresIn: exp,
  };

  return new Promise((resolve, reject) => {
    jsonwebtoken.sign(pay, key, opt, (error, token) => {
      return error ? reject(error) : resolve(token);
    });
  });
};

// 驗證
exports.verify = (token, key = publicKey) => {
  const opt = {
    algorithms: jwtAlg,
  };

  return new Promise((resolve, reject) => {
    jsonwebtoken.verify(token, key, opt, (error, payload)=>{
      return error ? reject(error) : resolve(payload);
    });
  });
};
