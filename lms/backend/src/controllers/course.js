const api = require('../request/api/index');
const GeneralError = require('../helpers/error');

exports.myCourse = async (req, res, next) => {
  // 取得身分token中紀錄的Patient ID
  const {patientId} = req.thisUser.jwtPayload;
  // http resonpse
  let response;
  // FHIR resource
  let appointments;
  // response array data
  let responseData = [];

  try {
    response = await api.fhir.get(`Appointment?patient=${patientId.join()}&_elements=description`);
    if (response.data.total === 0) {
      return res.status(200).json({success: true, data: responseData});
    }
    appointments = [...response.data.entry];

    // 篩選並組合出需要的資料
    responseData = appointments.map((a) => {
      const appointment = a.resource;

      const id = appointment.id;
      const name = appointment.description;

      return {id, name};
    });

    return res.status(200).json({success: true, data: responseData});
  } catch (error) {
    return next(error);
  }
};

exports.courseContent = async (req, res, next) => {
  // 取得身分token中紀錄的Patient ID
  const {patientId} = req.thisUser.jwtPayload;
  // 取得課程ID
  const {courseId} = req.params;
  // http resonpse
  let response = [];
  // FHIR resource
  let appointment; let documentReference;
  // response array data
  let responseData = {};

  try {
    // 檢查用戶是否有參與該課程
    response = await api.fhir.get(`Appointment?_id=${courseId}&patient=${patientId.join()}&_elements=description`);
    if (response.data.total === 0) {
      const newError = new GeneralError('AUTH-0003');
      return next(newError);
    }
    appointment = response.data.entry[0].resource;

    // 取得課程的教材清單
    response = await api.fhir.get(`DocumentReference?_has:Appointment:supporting-info:_id=${courseId}`);
    if (response.data.total === 0) {
      return res.status(200).json({success: true, data: responseData});
    }
    documentReference = response.data.entry[0].resource;

    // 擷取教材內容
    const content = {};
    if (documentReference.content) {
      documentReference.content.forEach((dr) => {
        const chapterNumber = dr.format.code;
        if (!content[chapterNumber]) {
          content[chapterNumber] = [];
        }
        content[chapterNumber].push(dr.attachment);
      });
    }

    responseData = {
      course_id: appointment.id,
      course_name: appointment.description,
      content,
    };

    return res.status(200).json({success: true, data: responseData});
  } catch (error) {
    return next(error);
  }
};
