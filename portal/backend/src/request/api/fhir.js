const axios = require('../http');
const config = require('../../config/index');

const option = {
  headers: {},
};

// FHIR GET API
exports.get = (query = '', cache = false) => {
  const opt = option;
  if (!cache) opt.headers['Cache-Control'] = 'no-cache';

  return axios.get(`${config.fhirServer}/${query}`, opt);
};

// FHIR POST API
exports.post = (query = '', params = {}, cache = false) => {
  const opt = option;
  if (!cache) opt.headers['Cache-Control'] = 'no-cache';

  return axios.post(`${config.fhirServer}/${query}`, params, opt);
};

// FHIR PUT API
exports.put = (query = '', params = {}, cache = false) => {
  const opt = option;
  if (!cache) opt.headers['Cache-Control'] = 'no-cache';

  return axios.put(`${config.fhirServer}/${query}`, params, opt);
};

// FHIR DELETE API
exports.delete = (query = '', cache = false) => {
  const opt = option;
  if (!cache) opt.headers['Cache-Control'] = 'no-cache';

  return axios.delete(`${config.fhirServer}/${query}`, opt);
};
