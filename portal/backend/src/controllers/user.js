const api = require('../request/api/index');
const config = require('../config/index');
const validateSchema = require('../helpers/validateSchema');
const GeneralError = require('../helpers/error');
const jwt = require('../helpers/jwt');
const sendEmail = require('../helpers/sendEmail');

exports.isLogin = async (req, res, next) => {
  return res.status(200).json({success: true});
};

exports.logIn = async (req, res, next) => {
  // 資料驗證
  const schema = validateSchema.userSignIn;
  const validateResult = schema.validate(req.body, {stripUnknown: true});
  if (validateResult.error) {
    const newError = new GeneralError('LOGIN-0001');
    newError.data = validateResult.error.details[0].message;
    return next(newError);
  }

  // 取得輸入資料
  const {username, password} = validateResult.value;
  // http response
  let response;
  // FHIR resource
  let person;

  try {
    // 用帳號(username)密碼(password)驗證用戶是否存在
    response = await api.fhir.get(`Person?identifier=username|${username}&identifier=password|${password}`);
    // 帳號密碼正確則回傳一筆資料
    if (response.data.total !== 1) {
      const newError = new GeneralError('LOGIN-0001');
      return next(newError);
    }
    person = response.data.entry[0].resource;

    // 用戶存在
    // 分類用戶具有的PatientId及PractitionerId
    const patientId = [];
    const practitionerId = [];
    person.link.forEach((link) => {
      const id = link.target.reference.split('/')[1];
      link.target.type === 'Patient' ? patientId.push(id) : practitionerId.push(id);
    });
    // 核發Token並回傳
    const payload = {
      aud: config.secret.jwt.audience,
      personName: person.name[0].text,
      personId: person.identifier.find((ide) => ide.system === 'personId').value,
      practitionerId,
      patientId,
    };
    const token = await jwt.sign(payload);

    res.cookie('Authentication', 'Bearer ' + token, {httpOnly: true});
    return res.status(201).json({success: true, token: token});
  } catch (error) {
    return next(error);
  }
};

exports.signUp = async (req, res, next) => {
  // 資料驗證
  const schema = validateSchema.userSignUp;
  const validateResult = schema.validate(req.body, {stripUnknown: true});
  if (validateResult.error) {
    const newError = new GeneralError('SIGNUP-0001');
    newError.data = validateResult.error.details[0].message;
    return next(newError);
  }

  // 取得輸入資料
  const {username, password, name, email, role, organization, grade, studentId, department, position} = validateResult.value;
  // http response
  let response;
  // FHIR resource
  let bundle; let person; let patient; let practitioner;

  try {
    // 檢查帳號與信箱是否已被使用
    bundle = {
      'resourceType': 'Bundle',
      'type': 'transaction',
      'entry': [
        {
          'request': {
            'method': 'GET',
            'url': `/Person?identifier=username|${username}&_summary=count`,
          },
        },
        {
          'request': {
            'method': 'GET',
            'url': `/Person?email=${email}&_summary=count`,
          },
        },
      ],
    };
    response = await api.fhir.post('', bundle);
    const isUsername = response.data.entry[0].resource.total;
    const isEmail = response.data.entry[1].resource.total;
    // 如果回傳的Person資料大於等於一筆則代表被使用
    if (isUsername >= 1) {
      const newError = new GeneralError('INPUT-0002');
      return next(newError);
    }
    if (isEmail >= 1) {
      const newError = new GeneralError('INPUT-0003');
      return next(newError);
    }

    // 取得資料庫的最後一筆PersonId，+1後為新的PersonId
    response = await api.fhir.get('Person?_sort=-_id&_count=1&_elements=identifier');
    const thisYear = new Date().getFullYear() - 1911;
    let personId;
    if (response.data.total === 0) {
      personId = `${thisYear}0001`;
    } else {
      if (!response.data.entry[0].resource.identifier.find((ide) => ide.system === 'personId')) {
        personId = `${thisYear}0001`;
      } else {
        personId = response.data.entry[0].resource.identifier.find((ide) => ide.system === 'personId').value;
        personId = thisYear == personId.substr(0, 3) ? parseInt(personId) + 1 : `${thisYear}0001`;
      }
    }

    // 創建FHIR Person、Patient、Practitioner
    practitioner = {
      'resourceType': 'Practitioner',
      'identifier': [
        {'system': 'personId', 'value': personId},
      ],
    };

    patient = {
      'resourceType': 'Patient',
      'identifier': [
        {'system': 'personId', 'value': personId},
        {'system': 'role', 'value': role},
        {'system': 'organization', 'value': organization},
      ],
    };
    // 依照角色不同要求不同的資料，ex:學生要求學號、職員要求職位
    switch (role) {
    case 1: // 國高中
      patient.identifier.push({system: 'grade', value: grade});
      patient.identifier.push({system: 'sid', value: studentId});
      break;
    case 2: // 大學以上
      patient.identifier.push({system: 'grade', value: grade});
      patient.identifier.push({system: 'sid', value: studentId});
      patient.identifier.push({system: 'department', value: department});
      break;
    case 3: // 教師
      patient.identifier.push({system: 'position', value: position});
      break;
    case 4: // 職員
      patient.identifier.push({system: 'position', value: position});
      break;
    }

    person = {
      'resourceType': 'Person',
      'identifier': [
        {'system': 'username', 'value': username},
        {'system': 'password', 'value': password},
        {'system': 'personId', 'value': personId},
      ],
      'name': [{'text': name}],
      'telecom': [{'system': 'email', 'value': email}],
      'link': [
        {'target': {'reference': `Patient?identifier=personId|${personId}`, 'type': 'Patient'}},
        {'target': {'reference': `Practitioner?identifier=personId|${personId}`, 'type': 'Practitioner'}},
      ],
    };

    // 單獨先POST Person
    await api.fhir.post(`Person`, person);
    // 組成Bundle批次POST Patient、Practitioner
    bundle = {
      'resourceType': 'Bundle',
      'type': 'transaction',
      'entry': [
        {
          'resource': patient,
          'request': {
            'method': 'POST',
            'url': '/Patient',
          },
        },
        {
          'resource': practitioner,
          'request': {
            'method': 'POST',
            'url': '/Practitioner',
          },
        },
      ],
    };

    await api.fhir.post('', bundle);

    return res.status(201).json({success: true});
  } catch (error) {
    return next(error);
  }
};

exports.detail = async (req, res, next) => {
  // 取得身分token中紀錄的person ID
  const {personId} = req.thisUser.jwtPayload;
  // http response
  let response;

  try {
    response = await api.fhir.get(`Patient?_revinclude=Person:patient&_has:Person:patient:identifier=${personId}`);

    const [person, patient] = await (async () => {
      const Person = {}; const Patient = {};
      for (entry of response.data.entry) {
        switch (entry.resource.resourceType) {
        case 'Person':
          Person.name = entry.resource.name[0].text;
          Person.email = entry.resource.telecom.find((tel) => tel.system === 'email').value;
          break;
        case 'Patient':
          Patient.organization = entry.resource.identifier.find((ide) => ide.system === 'organization').value;
          break;
        }
      }
      return [Person, Patient];
    })();

    const resData = {
      name: person.name,
      email: person.email,
      organization: patient.organization,
    };

    return res.status(200).json({success: true, data: resData});
  } catch (error) {
    return next(error);
  }
};

exports.update = async (req, res, next) => {
  // 資料驗證
  const schema = validateSchema.userUpdate;
  const validateResult = schema.validate(req.body, {stripUnknown: true});
  if (validateResult.error) {
    const newError = new GeneralError('INPUT-0001');
    newError.data = validateResult.error.details[0].message;
    return next(newError);
  }

  const {personId} = req.thisUser.jwtPayload;
  const {name, password, email, organization} = validateResult.value;
  let response;

  try {
    // 取得舊資料
    response = await api.fhir.get(`Patient?_revinclude=Person:patient&_has:Person:patient:identifier=${personId}`);
    const originalPerson = response.data.entry[1].resource;
    const originalPatient = response.data.entry[0].resource;
    // 新資料
    const newPerson = {...originalPerson};
    const newPatient = {...originalPatient};

    // 檢查email是否有被使用過
    response = await api.fhir.get(`Person?email=${email}`);
    // 如果Person回傳數量 != 0 且 該PersonID非自己則代表已被別人使用過
    const isEmailUsed = response.data.total !== 0 && response.data.entry[0].resource.id !== originalPerson.id;
    if (isEmailUsed) {
      const newError = new GeneralError('INPUT-0003');
      return next(newError);
    }

    // 修改姓名
    newPerson.name[0].text = name;
    // 修改email
    const emailIndex = newPerson.telecom.findIndex((tel) => tel.system === 'email');
    newPerson.telecom[emailIndex].value = email;
    // 修改密碼
    if (password !== '') {
      const passwordIndex = newPerson.identifier.findIndex((ide) => ide.system === 'password');
      newPerson.identifier[passwordIndex].value = password;
    }
    // 修改組織
    const organizationIndex = newPatient.identifier.findIndex((ide) => ide.system === 'organization');
    newPatient.identifier[organizationIndex].value = organization;

    // 組合FHIR Bundle批次上傳Person、Patient
    const bundle = {
      'resourceType': 'Bundle',
      'type': 'transaction',
      'entry': [
        {
          'resource': newPatient,
          'request': {
            'method': 'PUT',
            'url': `Patient/${newPatient.id}`,
          },
        },
        {
          'resource': newPerson,
          'request': {
            'method': 'PUT',
            'url': `Person/${newPerson.id}`,
          },
        },
      ],
    };
    // 執行上傳
    response = await api.fhir.post('', bundle);
  } catch (error) {
    return next(error);
  }

  return res.status(201).json({success: true});
};

exports.logOut = async (req, res, next) => {
  res.clearCookie('Authentication');
  res.status(201).json({success: true});
  return;
};

exports.forget = async (req, res, next) => {
  // 資料驗證
  const schema = validateSchema.userForget;
  const validateResult = schema.validate(req.body, {stripUnknown: true});
  if (validateResult.error) {
    const newError = new GeneralError('INPUT-0001');
    newError.data = validateResult.error.details[0].message;
    return next(newError);
  }

  // 取得輸入資料
  const {email} = validateResult.value;
  // http response
  let response;
  // person data
  let person;

  try {
    // 依照email取得用戶(Person)
    response = await api.fhir.get(`Person?email=${email}`);
    // 回傳的Person數量等於0代表沒有這個Email所屬的用戶
    if (response.data.total === 0) {
      const newError = new GeneralError('FORGET-0001');
      return next(newError);
    }
    person = response.data.entry[0].resource;

    // 核發重設密碼Token
    const payload = {
      subject: 'ResetPassWord',
      aud: config.secret.jwt.issuer,
      personId: person.id,
    };
    const token = await jwt.sign(payload, 5 * 60);

    // 寄信
    const mailOption = {
      to: email,
      subject: 'MITW 重設密碼通知',
      html: `您好，請於5分鐘內至以下鏈結重設密碼：<a href=${config.systemUrl.portalClient}/forget-reset?token=${token}>重設密碼鏈結</a>`,
    };
    await sendEmail(mailOption);

    return res.status(201).json({success: true});
  } catch (error) {
    return next(error);
  }
};

exports.forgetReset = async (req, res, next) => {
  // 資料驗證
  const schema = validateSchema.userForgetReset;
  const validateResult = schema.validate(req.body, {stripUnknown: true});
  if (validateResult.error) {
    const newError = new GeneralError('INPUT-0001');
    newError.data = validateResult.error.details[0].message;
    return next(newError);
  }

  // 取得身分token中紀錄的訊息
  const {personId, subject} = req.thisUser.jwtPayload;
  // 取得輸入資料
  const {password} = validateResult.value;
  // http response
  let response;

  // 檢查Subject是否為ResetPassWord，判斷攜帶的Token是否為用來重設密碼的
  if (subject !== 'ResetPassWord') {
    const newError = new GeneralError('AUTH-0003');
    return next(newError);
  }

  try {
    // 取得用戶資料(Person)
    response = await api.fhir.get(`Person/${personId}`);
    const newPerson = response.data;
    const passwordIndex = newPerson.identifier.findIndex((ide) => ide.system === 'password');
    newPerson.identifier[passwordIndex].value = password;

    // 上傳資料
    await api.fhir.put(`Person/${personId}`, newPerson);

    return res.status(201).json({success: true});
  } catch (error) {
    return next(error);
  }
};
