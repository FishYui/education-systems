const GeneralError = require('../helpers/error');

module.exports = (err, req, res, next) => {
  // 輸出錯誤訊息
  console.warn(err);

  if (err instanceof GeneralError) {
    return res.status(err.statusCode).json({
      success: false,
      errorCode: err.errorCode,
      message: err.message,
      data: err.data,
    });
  }

  return res.status(500).json({
    success: false,
    message: err.message,
  });
};
