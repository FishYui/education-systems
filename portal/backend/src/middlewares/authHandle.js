const jwt = require('../helpers/jwt');
const GeneralError = require('../helpers/error');

module.exports = async (req, res, next) => {
  // 取得header或cookie上的Token
  const authToken = req.headers.authorization || req.cookies.Authentication;

  // 如果沒Token回報錯誤
  if (!authToken) {
    const newError = new GeneralError('AUTH-0001');
    return next(newError);
  }

  // 如果Token的型態有誤回報錯誤 Token Type: 「Bearer TokenValue」
  const [type, token] = authToken.split(' ');
  if (type != 'Bearer' || !token) {
    const newError = new GeneralError('AUTH-0002');
    return next(newError);
  }

  try {
    // 檢查Token是否合法、可正確驗證
    const payload = await jwt.verify(token);
    req.thisUser = {
      jwtPayload: payload,
    };
  } catch (error) {
    // Token不合法、驗證失敗回報錯誤
    let newError;
    if (error.name === 'TokenExpiredError') {
      newError = new GeneralError('AUTH-0002');
    } else {
      newError = new GeneralError('AUTH-0001');
    }
    return next(newError);
  }

  next();
};
