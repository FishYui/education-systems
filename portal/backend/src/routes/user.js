const express = require('express');
const router = express.Router();

// middlewares
const AuthHandle = require('../middlewares/authHandle');

// controllers
const userControllers = require('../controllers/user');
// http://localhost/user/isLogin
// /user/isLogin
router.get('/isLogin',
  AuthHandle,
  userControllers.isLogin);

// /user/login  => 登入
router.post('/login', userControllers.logIn);

// /user/signup  => 註冊
router.post('/signup', userControllers.signUp);

// /user/logout  => 登出
router.post('/logout', userControllers.logOut);

// /user/forget  => 忘記密碼
router.post('/forget', userControllers.forget);

// /user/forget-reset  => 忘記密碼後修改密碼
router.put('/forget-reset',
  AuthHandle,
  userControllers.forgetReset);

// /user/  => 取得個人帳號資訊
router.get('/',
  AuthHandle,
  userControllers.detail);

// /user/  => 修改個人帳號資訊
router.put('/',
  AuthHandle,
  userControllers.update);

module.exports = router;
