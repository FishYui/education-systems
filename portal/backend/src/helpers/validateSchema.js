const Joi = require('joi');

module.exports = {

  userSignIn: Joi.object({
    username: Joi.string()
      .required(),
    password: Joi.string()
      .regex(/[a-zA-Z0-9]{3,30}/)
      .required(),
  }),

  userSignUp: Joi.object({
    username: Joi.string()
      .required(),
    password: Joi.string()
      .regex(/[a-zA-Z0-9]{3,30}/)
      .required(),
    name: Joi.string()
      .required(),
    email: Joi.string()
      .email()
      .required(),
    role: Joi.number()
      .required(),
    organization: Joi.string()
      .required(),
    grade: Joi.number()
      .when('role', {
        is: 1,
        then: Joi.required(),
      })
      .concat(Joi.number()
        .when('role', {
          is: 2,
          then: Joi.required(),
        })),
    sid: Joi.string()
      .when('role', {
        is: 1,
        then: Joi.required(),
      })
      .concat(Joi.string()
        .when('role', {
          is: 2,
          then: Joi.required(),
        })),
    department: Joi.string()
      .when('role', {
        is: 2,
        then: Joi.required(),
      }),
    position: Joi.string()
      .when('role', {
        is: 3,
        then: Joi.required(),
      })
      .concat(Joi.string()
        .when('role', {
          is: 4,
          then: Joi.required(),
        })),
  }),

  userUpdate: Joi.object({
    name: Joi.string()
      .required(),
    email: Joi.string()
      .email()
      .required(),
    organization: Joi.string()
      .required(),
    password: Joi.string()
      .regex(/[a-zA-Z0-9]{3,30}/)
      .allow(''),
  }),

  userForget: Joi.object({
    email: Joi.string()
      .email()
      .required(),
  }),

  userForgetReset: Joi.object({
    password: Joi.string()
      .regex(/[a-zA-Z0-9]{3,30}/)
      .required(),
  }),
};
