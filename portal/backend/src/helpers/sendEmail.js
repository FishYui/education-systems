const nodemailer = require('nodemailer');
const config = require('../config/index');

module.exports = (option) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.adminEmail.account,
      pass: config.adminEmail.password,
    },
  });

  const mailOptions = {
    ...option,
    from: config.adminEmail.account,
  };

  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (error, info) => {
      return error ? reject(error) : resolve(true);
    });
  });
};
