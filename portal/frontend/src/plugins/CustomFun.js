export default {
  async fetch(url, opt) {
    try {
      const option = opt || {};
      const response = await fetch(url, option);

      if(!response.ok) {
        await this.fetch_err(response);
        return { success: false };
      }
      else {
        const contentType = response.headers.get('Content-Type');
        if(contentType == null)
          return await response.text();
        if(contentType.indexOf('json') > -1)
          return await response.json();
      }
    }
    catch(e) {
      alert('伺服器連線錯誤，請稍後再使用或回報錯誤。');
      return { success: false };
    }
  },
  async fetch_err(response) {

    const result = await response.json() || '';

    switch(response.status) {
      case 401:
        alert(result.message);
        window.location.href = process.env.VUE_APP_PORTAL_CLIENT + "/login";
        break;
      case 404:
        alert('404 Not Found。');
        break;
      case 500:
        alert('伺服器錯誤，請稍後再使用或回報錯誤。');
        break;
      default:
        alert(result.message);
        break;
    }
  }
}