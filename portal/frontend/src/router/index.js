import Vue from "vue";
import VueRouter from "vue-router";
import CustomFun from "../plugins/CustomFun";

//view
//public
import PublicIndex from "../views/public/Index.vue";
import LoginPage from "../views/public/Login.vue";
import SignupPage from "../views/public/Signup.vue";
import ForgetPage from "../views/public/Forget.vue";
import ForgetResetPage from "../views/public/ForgetReset.vue";
//private
import PrivateIndex from "../views/private/Index.vue";
import HomePage from "../views/private/Home.vue";
import UserInfoPage from "../views/private/UserInfo.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: PrivateIndex,
    children: [
      {
        path: "/",
        name: "Home",
        component: HomePage,
        meta: {
          auth: true
        }
      },
      {
        path: "/userinfo",
        name: "UserInfo",
        component: UserInfoPage,
        meta: {
          auth: true
        }
      }
    ]
  },
  {
    path: "/",
    component: PublicIndex,
    children: [
      {
        path: "/login",
        name: "Login",
        component: LoginPage
      },
      {
        path: "/signup",
        name: "Signup",
        component: SignupPage
      },
      {
        path: "/forget",
        name: "Forget",
        component: ForgetPage
      },
      {
        path: "/forget-reset",
        name: "ForgetReset",
        component: ForgetResetPage
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  if(to.meta.auth === true) {
    const auth_res = await CustomFun.fetch(`${process.env.VUE_APP_PORTAL_SERVER}/user/isLogin`, { credentials: 'include' });
    if(auth_res.success) 
      next();
  }
  else
    next();
});

export default router;
