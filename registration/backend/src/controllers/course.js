const api = require('../request/api/index');
const GeneralError = require('../helpers/error');

exports.getAll = async (req, res, next) => {
  // http resonpse
  let response;
  // FHIR resource
  let appointments;
  // response array data
  let responseData = [];
  try {
    response = await api.fhir.get(`Appointment?_elements=status,description,supportingInformation&_sort=_id`);
    if (response.data.total === 0) {
      return res.status(200).json({success: true, data: responseData});
    }
    appointments = [...response.data.entry];

    responseData = appointments.map((a) => {
      const appointment = a.resource;

      const id = appointment.id;
      const name = appointment.description;
      const status = appointment.status == 'booked' ? true : false;
      const detailIndex = appointment.supportingInformation ? appointment.supportingInformation.findIndex((supportingInformation) => supportingInformation.type === 'CourseDetail') : -1;
      const detail = detailIndex !== -1 ? appointment.supportingInformation[detailIndex].display : '';

      return {id, name, status, detail};
    });
    return res.status(200).json({success: true, data: responseData});
  } catch (error) {
    return next(error);
  }
};

exports.getMy = async (req, res, next) => {
  // 取得身分token中紀錄的Patient ID
  const {patientId} = req.thisUser.jwtPayload;
  // http resonpse
  let response;
  // FHIR resource
  let appointments;
  // response array data
  let responseData = [];

  try {
    response = await api.fhir.get(`Appointment?patient=${patientId.join()}`);
    if (response.data.total === 0) {
      return res.status(200).json({success: true, data: responseData});
    }
    appointments = [...response.data.entry];

    responseData = appointments.map((a) => {
      const appointment = a.resource;

      const id = appointment.id;
      const name = appointment.description;

      return {id, name};
    });
    return res.status(200).json({success: true, data: responseData});
  } catch (error) {
    return next(error);
  }
};

exports.register = async (req, res, next) => {
  // 取得課程ID
  const {courseId} = req.query;
  // 取得身分token中紀錄的Person Name、Patient ID
  const {personName, patientId} = req.thisUser.jwtPayload;
  // http resonpse
  let response;
  // FHIR resource
  let appointment;

  try {
    // 取得課程報名現況資料
    response = await api.fhir.get(`Appointment?_id=${courseId}`);
    appointment = response.data.entry[0].resource;

    // 檢查appointment resource有無participant(報名表)，沒有的話新增
    if (!appointment.participant) appointment.participant = [];

    // 檢查用戶是否已經報名過
    const patientIndex = appointment.participant.findIndex((par) => par.actor.reference.split('/')[1] == patientId[0]);
    // 有 => 回報已報名
    if (patientIndex !== -1) {
      const newError = new GeneralError('REGISTRATION-0001');
      return next(newError);
    }
    // 沒有 => 新增資料進報名表
    appointment.participant.push({
      actor: {
        reference: `Patient/${patientId[0]}`,
        display: personName,
      },
    });
    // 更新資料
    await api.fhir.put(`Appointment/${courseId}`, appointment);

    return res.status(201).json({success: true});
  } catch (error) {
    return next(error);
  }
};

exports.unRegister = async (req, res, next) => {
  // 取得課程ID
  const {courseId} = req.query;
  // 取得身分token中紀錄的Patient ID
  const {patientId} = req.thisUser.jwtPayload;
  // http resonpse
  let response;
  // FHIR resource
  let appointment;

  try {
    // 取得課程報名現況資料
    response = await api.fhir.get(`Appointment?_id=${courseId}`);
    appointment = response.data.entry[0].resource;

    // 檢查appointment resource有無participant(報名表)
    // 沒有的話直接response success
    if (!appointment.participant) return res.status(201).json({success: true});

    // 找尋用戶的報名位置
    const patientIndex = appointment.participant.findIndex((par) => par.actor.reference.split('/')[1] == patientId[0]);
    // 有找到則把該位置資料移除
    if (patientIndex !== -1) appointment.participant.splice(patientIndex, 1);
    // 更新資料
    await api.fhir.put(`Appointment/${courseId}`, appointment);

    return res.status(201).json({success: true});
  } catch (error) {
    return next(error);
  }
};
