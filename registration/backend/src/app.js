const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const config = require('./config/index');
const routes = require('./routes/index');
const errorHandle = require('./middlewares/errorHandle');

const app = express();

app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors({
  origin: [config.systemUrl.portalClient, config.systemUrl.regrationClient],
  credentials: true,
}));

routes(app);

// 錯誤處理middleware
app.use(errorHandle);

app.listen(config.port, () => {
  console.log(`Server Started on PORT: ${config.port}.`);
});
