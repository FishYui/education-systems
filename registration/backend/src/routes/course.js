const express = require('express');
const router = express.Router();

// middlewares
const AuthHandle = require('../middlewares/authHandle');

// controllers
const courseControllers = require('../controllers/course');

// /course/  => 取得所有課程清單
router.get('/',
  AuthHandle,
  courseControllers.getAll);

// /course/my  => 取得個人已報名的課程清單
router.get('/my',
  AuthHandle,
  courseControllers.getMy);

// /course/register  => 用戶報名課程
router.post('/register',
  AuthHandle,
  courseControllers.register);

// /course/unRegister  => 用戶取消報名課程
router.post('/unRegister',
  AuthHandle,
  courseControllers.unRegister);

module.exports = router;
