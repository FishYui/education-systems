const {default: axios} = require('axios');

const instance = axios.create({timeout: 1000*10});

instance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.error(error);
  },
);

instance.interceptors.response.use(
  (res) => {
    return res.status >= 200 && res.status < 300 ? Promise.resolve(res) : Promise.reject(res);
  },
  (error) => {
    const {response} = error;
    if (response) {
      // 請求已發出，但是不在2XX的範圍
      return Promise.reject(response);
    } else {
      return Promise.reject(error);
    }
  },
);

module.exports = instance;
