import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import CourseList from "../views/course/List.vue";
import MyCourse from "../views/course/My.vue";
import CustomFun from "../plugins/CustomFun";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      auth: true
    }
  },
  {
    path: "/course-list",
    name: "CourseList",
    component: CourseList,
    meta: {
      auth: true
    }
  },
  {
    path: "/my-course",
    name: "MyCourse",
    component: MyCourse,
    meta: {
      auth: true
    }
  },
  {
    path: "*",
    redirect: "/"
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {

  if(to.query.token) {
    await CustomFun.fetch(`${process.env.VUE_APP_REGISTION_SERVER}/login`, {
      method: "POST",
      headers: {
        "content-type": "application/json",
        "authorization": to.query.token
      },
      credentials: 'include'
    });
  }

  if(to.meta.auth === true) {
    const auth_res = await CustomFun.fetch(`${process.env.VUE_APP_REGISTION_SERVER}/user/isLogin`, { credentials: 'include' });
    if(auth_res.success) 
      next();
  }
  else
    next();
});

export default router;
