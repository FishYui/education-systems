module.exports = {
  devServer: {
    port: process.env.VUE_APP_PORT,
  },
  runtimeCompiler: true,
}