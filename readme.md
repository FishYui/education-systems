# 學習系統 

### 具有以下幾個子系統
- portal：入口網站
- lms：學習管理系統
- registration：選課系統

### 執行方式
- backend：
  ```
    $ npm install
    $ npm run dev
  ```
- frontend：
  ```
    $ npm install
    $ npm run serve
  ```

### 文檔說明
[Link](https://drive.google.com/file/d/1FwX4n7X9bDtrkrgaojWVB0pd7omS-QLt/view)

### 公開測試網站
[Link](http://203.64.84.150:8001/login)
